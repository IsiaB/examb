import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CityService {

  constructor(private http:HttpClient, private afs: AngularFirestore) { }
  private IMP="units=metric";
  getWeatherData(city) {
    return this.http.get('http://api.openweathermap.org/data/2.5/weather?q=' + city.name.toLowerCase() + '&appid=b66167516e99e56eebe4c9cb68e00d79&' + this.IMP)
  }

  predict(city) {
    const lamdaUrl = 'https://q975c9ma8c.execute-api.us-east-1.amazonaws.com/beta';
    return this.http.post(lamdaUrl, JSON.stringify({temp:city.temp, humidity: city.humidity}))
  }

  save(data) {
    return this.afs.doc('cities/cities').set({data})
  }
  getCities() {
    return this.afs.doc('cities/cities').valueChanges();
  }
}
