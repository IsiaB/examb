import { CityService } from './city.service';
import { Component, OnInit } from '@angular/core';
import { WeatherInterface } from './weather.interface';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  cities;
  tableColumns: string[] = ['Name', 'Humidity', 'Temputare', 'Will it Rain?'];

  constructor(private cityService: CityService) { }

  ngOnInit(): void {
    this.cities = [
      {name: 'London', humidity: null, temp: null, rain: null, image: null },
      {name: 'Paris', humidity: null, temp: null, rain: null, image: null },
      {name: 'Tel Aviv', humidity: null, temp: null, rain: null, image: null },
      {name: 'Jerusalem', humidity: null, temp: null, rain: null, image: null },
      {name: 'Berlin', humidity: null, temp: null, rain: null, image: null },
      {name: 'Rome', humidity: null, temp: null, rain: null, image: null },
      {name: 'Dubai', humidity: null, temp: null, rain: null, image: null },
      {name: 'Athens', humidity: null, temp: null, rain: null, image: null }
    ];

    this.getDbData();
  }

  getDbData() {
    this.cityService.getCities().subscribe((data:any) => {
      if (data){
        this.cities = data.data;
      }
    })
  }

  getWeatherData(city) {
    this.cityService.getWeatherData(city).subscribe((data:WeatherInterface) => {
      city.temp = data.main.temp;
      city.humidity = data.main.humidity;
      city.image = 'https://api.openweathermap.org/img/w/' + data.weather[0].icon + '.png';
    })
  }

  predict(city){
    this.cityService.predict(city).subscribe((data:any) => {
      if (data.result > 50) {
        city.rain = 'Will Rain';
      } else {
        city.rain = 'Will NOT Rain';
      }
    })
  }

  save(){
    this.cityService.save(this.cities).then(() => {
      
    });
  }

}
