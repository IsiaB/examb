// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDmHEOnPM65ZCfJrKVsNFW_KHJLZUbH8rE",
    authDomain: "examb-isia.firebaseapp.com",
    projectId: "examb-isia",
    storageBucket: "examb-isia.appspot.com",
    messagingSenderId: "663949375231",
    appId: "1:663949375231:web:c3b3e053e2417fbc813eca"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
